﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    internal List<ItemData> Items=new List<ItemData>();
    public List<ItemData> ToFindItems = new List<ItemData>();
    public List<GameObject> PlayerItems = new List<GameObject>();
    public AudioSource ItemSound;
    internal UIController MyUIController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddItem(ItemData item)
    {
        Items.Add(item);
        PlayerItems[ToFindItems.IndexOf(item)].SetActive(true);
        MyUIController.ReloadItems();
    }

    public void InitUIController(UIController newController) {
        MyUIController = newController;
    }

    public bool HaveItem(int position)
    {
        return Items.Contains(ToFindItems[position]);
    }

    public bool HaveKey()
    {
        foreach (ItemData item in Items)
        {
            if (item.GetType() == typeof(KeyItemData)) {
                if (((KeyItemData)item).Type == KeyItemType.Key)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
