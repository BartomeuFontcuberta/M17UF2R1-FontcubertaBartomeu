﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class MyPlayerController : MonoBehaviour
{
    private CharacterController controller;
    private Animator animator;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    public float playerSpeed = 2.0f;
    public float playerSpeedRotation = 2.0f;
    private float jumpHeight = 1.0f;
    private float gravityValue = -9.81f;
    private bool inCelebration=false;
    private GameObject onItem = null;
    public CinemachineVirtualCamera CelebrationCamera;
    public CinemachineVirtualCamera AimCamera;
    public GameObject Weapon;
    private bool onCatching = false;

    private void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
        animator = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        if (!inCelebration)
        {
            groundedPlayer = controller.isGrounded;
            if (groundedPlayer && playerVelocity.y < 0)
            {
                playerVelocity.y = -1f;
            }

            Vector3 move = new Vector3(0, 0, Input.GetAxis("Vertical"));
            Vector3 rotation = new Vector3(0, Input.GetAxis("Horizontal"), 0);

            animator.SetFloat("Speed", Input.GetAxis("Vertical"));
            animator.SetFloat("Rotation", Input.GetAxis("Horizontal"));

            if (Input.GetButton("Run") && move.z > 0)
            {
                animator.SetFloat("Speed", Input.GetAxis("Vertical") + 1);
                move = new Vector3(0, 0, Input.GetAxis("Vertical") + 1);
                rotation *= 2;
            }
            move = transform.TransformDirection(move);

            animator.SetBool("Crouch", Input.GetButton("Crouch"));
            animator.SetBool("Aim", Input.GetButton("Aim"));
            if (animator.GetBool("Weapon"))
            {
                if (Input.GetButton("Aim"))
                {
                    AimCamera.Priority = 12;
                }
                else
                {
                    AimCamera.Priority = 5;
                }
            }
            else
            {
                AimCamera.Priority = 5;
            }
            if (Input.GetButtonDown("OutWeapon") && !Input.GetButton("Crouch"))
            {
                ActiveWeaponLayer();
                Weapon.SetActive(!animator.GetBool("Weapon"));
                animator.SetBool("Weapon", !animator.GetBool("Weapon"));
            }

            controller.Move(move * Time.deltaTime * playerSpeed);
            transform.Rotate(rotation * Time.deltaTime * playerSpeedRotation);

            if (onItem!=null&&Input.GetButtonDown("Pick"))
            {
                if (onItem.CompareTag("Item"))
                {
                    animator.SetTrigger("Pick");
                    ActiveWeaponLayer();
                    onCatching = true;
                }
            }

            if (Input.GetButtonDown("Celebration"))
            {
                animator.SetTrigger("Celebration");
                ActiveCelebrationLayer();
            }

            // Changes the height position of the player..
            if (Input.GetButtonDown("Jump") && groundedPlayer)
            {
                animator.SetTrigger("Jump");
                playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            }

            playerVelocity.y += gravityValue * Time.deltaTime;
            controller.Move(playerVelocity * Time.deltaTime);
        }
        else
        {
            CelebrationCamera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition += Time.deltaTime*2;
        }
    }

    private void ActiveWeaponLayer()
    {
        animator.SetLayerWeight(1, 1f);
    }
    private void DesactiveWeaponLayer()
    {
        animator.SetLayerWeight(1, 0);
    }
    private void ActiveCelebrationLayer()
    {
        inCelebration = true;
        animator.SetLayerWeight(2, 1f);
        CelebrationCamera.GetCinemachineComponent<CinemachineTrackedDolly>().m_PathPosition = 0;
        CelebrationCamera.Priority = 15;
    }
    private void DesactiveCelebrationLayer()
    {
        inCelebration = false;
        animator.SetLayerWeight(2, 0);
        CelebrationCamera.Priority = 5;
    }

    public void CatchItem()
    {
        onItem.GetComponent<ItemController>().Catched();
        onItem = null;
        onCatching = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "Damage":
                GameManager.Instance.MyUIController.ShowLosePanel();
                DestroyPlayer();
                break;
            case "Item":
                if (!onCatching)
                {
                    onItem = other.gameObject;
                }
                break;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (!onCatching)
        {
            onItem = null;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.CompareTag("Damage"))
        {
            GameManager.Instance.MyUIController.ShowLosePanel();
            DestroyPlayer();
        }
    }
    public void DestroyPlayer()
    {
        Destroy(gameObject);
    }
}
