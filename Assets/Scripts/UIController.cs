﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public GameObject ItemIconsPanel;
    public GameObject Item;
    private List<Image> itemIcons=new List<Image>();
    public GameObject NeedKeyText;
    public GameObject ResultPanel;
    public Text ResultText;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.InitUIController(this);
        for (int i = 0; i < GameManager.Instance.ToFindItems.Count; i++)
        {
            GameObject newItem = Instantiate(Item);
            newItem.transform.SetParent(ItemIconsPanel.transform);
            itemIcons.Add(newItem.GetComponent<Image>());
            itemIcons[i].sprite=GameManager.Instance.ToFindItems[i].Icon;
            newItem.GetComponent<RectTransform>().localScale = Vector3.one;
        }
        ReloadItems();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void ReloadItems()
    {
        for (int i=0; i< itemIcons.Count; i++)
        {
            if (GameManager.Instance.HaveItem(i))
            {
                itemIcons[i].color = Color.white;
            }
            else
            {
                itemIcons[i].color = Color.black;
            }
        }
    }

    public void ToRestart()
    {
        SceneManager.LoadScene(1);
    }

    public void ToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ShowWinPanel()
    {
        ResultPanel.SetActive(true);
        ResultText.text = "You win";
    }
    public void ShowLosePanel()
    {
        ResultPanel.SetActive(true);
        ResultText.text = "You lose";
    }
    public void ShowNeedKeyText()
    {
        NeedKeyText.SetActive(true);
        StartCoroutine(DissableKeyText());
    }
    IEnumerator DissableKeyText()
    {
        yield return new WaitForSeconds(2f);
        NeedKeyText.SetActive(false);
    }
}
