﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum KeyItemType
{
    Key
}
[CreateAssetMenu(fileName = "KeyItemData", menuName = "KeyItemData", order = 2)]
public class KeyItemData : ItemData
{
    public KeyItemType Type;
}
