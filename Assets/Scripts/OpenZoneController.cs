﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenZoneController : MonoBehaviour
{
    public Animator DoorAnimator;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")){
            if (GameManager.Instance.HaveKey())
            {
                DoorAnimator.SetTrigger("Open");
                Destroy(gameObject);
            }
            else
            {
                GameManager.Instance.MyUIController.ShowNeedKeyText();
            }
        }
    }
}
