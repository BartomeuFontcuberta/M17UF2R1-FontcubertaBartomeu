﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    public ItemData ItemData;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Catched()
    {
        GameManager.Instance.AddItem(ItemData);
        GameManager.Instance.ItemSound.PlayOneShot(ItemData.Sound);
        Destroy(gameObject.transform.parent.gameObject);
    }
}
