﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinZoneController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.MyUIController.ShowWinPanel();
            other.gameObject.GetComponent<MyPlayerController>().DestroyPlayer();
        }
    }
}
