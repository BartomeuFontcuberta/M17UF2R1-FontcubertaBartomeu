using System;
using UnityEngine;
using UnityEngine.AI;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform[] targets;                                    // target to aim for
        public float AttackDistance = 1;
        public float NoFollowDistance = 5;
        internal int objective=0;
        public Animator EnemyAnimator;
        public EnemyModes Mode;
        public EnemyStates State;
        public bool FoundPlayer;
        public enum EnemyModes
        {
            PlayerAndPositon,
            PlayerAndMultipleControl,
            Nothing,
            OnlyPlayer
        }
        public enum EnemyStates
        {
            Patrol,
            Follow,
            Attack
        }

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;
        }


        private void Update()
        {
            switch (Mode)
            {
                case EnemyModes.PlayerAndPositon:
                    PlayerAndPositonUpdate();
                    break;
                case EnemyModes.PlayerAndMultipleControl:
                    PlayerAndMultipleControlUpdate();
                    break;
                case EnemyModes.OnlyPlayer:
                    OnlyPlayerUpdate();
                    break;
            }
        }

        private void PlayerAndPositonUpdate()
        {
            if (Input.GetButtonDown("ChangeTarget"))
            {
                objective = 1 - objective;
            }
            setDestination(objective);
            moveTo();
            tryAttack();
        }
        private void PlayerAndMultipleControlUpdate()
        {
            switch (State)
            {
                case EnemyStates.Patrol:
                    patrolState();
                    break;
                case EnemyStates.Follow:
                    followState();
                    break;
                case EnemyStates.Attack:
                    attackState();
                    break;
            }
            
        }
        private void OnlyPlayerUpdate()
        {
            setDestination(0);
            moveTo();
            tryAttack();
        }

        private void patrolState()
        {
            if (objective == 0)
            {
                getNewDestination();
            }
            setDestination(objective);
            moveTo();
           // Debug.Log("Patrol: " + agent.remainingDistance);
            if (FoundPlayer)
            {
                State = EnemyStates.Follow;
            }
            else if (agent.remainingDistance < AttackDistance)
            {
                getNewDestination();
            }
        }

        private void getNewDestination()
        {
            int tmpObjective;
            do
            {
                System.Random rand = new System.Random();
                tmpObjective =rand.Next(1,targets.Length);
            } while (tmpObjective == objective);
            objective = tmpObjective;
        }

        private void followState()
        {
            setDestination(0);
            moveTo();
            Debug.Log("Follow: "+ agent.remainingDistance);
            if (agent.remainingDistance < AttackDistance) {
                EnemyAnimator.SetBool("Atack", true);
                State = EnemyStates.Attack;
            }else if (GetPathRemainingDistance() > NoFollowDistance)
            {
                FoundPlayer = false;
                State = EnemyStates.Patrol;
            }
        }

        private void attackState()
        {
            setDestination(0);
            moveTo();
            Debug.Log("Attack: " + agent.remainingDistance);
            if (agent.remainingDistance >= AttackDistance) {
                EnemyAnimator.SetBool("Atack", false);
                State = EnemyStates.Follow;
            }
        }

        private void moveTo()
        {
            if (agent.remainingDistance > agent.stoppingDistance)
            {
                character.Move(/*(targets[objective].position - transform.position)*/agent.desiredVelocity, false, false);
            }
            else
            {
                character.Move(Vector3.zero, false, false);
            }
        }
        private void tryAttack()
        {
            EnemyAnimator.SetBool("Atack",agent.remainingDistance < AttackDistance && objective == 0);
        }

        private void setDestination(int index)
        {
            if (targets[index] != null)
            {
                agent.SetDestination(targets[index].position);
            }
        }


        public void SetTarget(Transform target)
        {
            //this.target = target;
        }
        public float GetPathRemainingDistance()
        {
            if (agent.pathPending ||
                agent.pathStatus == NavMeshPathStatus.PathInvalid ||
                agent.path.corners.Length == 0)
                return -1f;

            float distance = 0.0f;
            for (int i = 0; i < agent.path.corners.Length - 1; ++i)
            {
                distance += Vector3.Distance(agent.path.corners[i], agent.path.corners[i + 1]);
            }

            return distance;
        }
    }
}
